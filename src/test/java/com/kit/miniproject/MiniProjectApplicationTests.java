package com.kit.miniproject;

import com.kit.miniproject.model.Subscription;
import com.kit.miniproject.model.SubscriptionStatus;
import com.kit.miniproject.model.SubscriptionTime;
import com.kit.miniproject.model.User;
import com.kit.miniproject.service.SubscriptionService;
import com.kit.miniproject.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import static com.kit.miniproject.util.TimeUtil.convertTimestampToLong;
import static com.kit.miniproject.util.TimeUtil.getCurrentTimeInTimestampFormat;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class MiniProjectApplicationTests {

    @Autowired
    private UserService userService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Test
    void contextLoads_Test() {
        System.out.println("######## Hello World! ########");
    }

    @Test
    void saveUser_readUser_Test() {
        User milad_user = new User();
        milad_user.setName("MiladHagh");
        milad_user.setPassword("123456");
        milad_user.setEmail("milad.haghparast@gmail.com");

        String saved_user = userService.save(milad_user);
		assertThat(saved_user.equals(milad_user.getName()));
		System.out.println("###### save user passed ######");

		User read_user = userService.read(milad_user.getName());
		assertThat(milad_user.getName().equals(read_user.getName()));
		System.out.println("###### read user passed ######");
	}


	@Test
	void saveUserSubscriptionTest() {
    	Timestamp timeBeforeSubscription = getCurrentTimeInTimestampFormat();

		User user_1 = new User();
		user_1.setName("MiladHagh");
		user_1.setPassword("123456");
		user_1.setEmail("milad.haghparast@gmail.com");
		userService.save(user_1);
		Subscription subscription_1 = new Subscription();
		subscription_1.setStatus(SubscriptionStatus.SUBSCRIBED);
		subscription_1.setUser(user_1);
		subscription_1.setSubscriptionDate(getCurrentTimeInTimestampFormat());
		Subscription subscription_1_saved = subscriptionService.save(subscription_1);
		SubscriptionStatus subscription_status_1 = subscriptionService.readSubscriptionStatus(subscription_1_saved);
		assertThat(subscription_status_1.equals(subscription_1_saved.getStatus()));

		User user_2 = new User();
		user_2.setName("MaedehGh");
		user_2.setPassword("123456");
		user_2.setEmail("maedeh.1989@gmail.com");
		userService.save(user_2);
		Subscription subscription_2 = new Subscription();
		subscription_2.setStatus(SubscriptionStatus.SUBSCRIBED);
		subscription_2.setUser(user_2);
		subscription_2.setSubscriptionDate(getCurrentTimeInTimestampFormat());
		Subscription subscription_2_saved = subscriptionService.save(subscription_2);
		SubscriptionStatus subscription_status_2 = subscriptionService.readSubscriptionStatus(subscription_2_saved);
		assertThat(subscription_status_2.equals(subscription_2_saved.getStatus()));

		Timestamp timeAfterSubscription = getCurrentTimeInTimestampFormat();

		List<Subscription> read_subscriptions_after = subscriptionService
				.readAllSubscribedByDate(timeBeforeSubscription, SubscriptionTime.AFTER);

		assertThat(read_subscriptions_after.size() == 2);

		List<Subscription> read_subscriptions_before = subscriptionService
				.readAllSubscribedByDate(timeAfterSubscription, SubscriptionTime.BEFORE);

		assertThat(read_subscriptions_before.size() == 2);
	}
}
