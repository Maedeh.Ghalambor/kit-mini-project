package com.kit.miniproject.service;

import com.kit.miniproject.model.Subscription;
import com.kit.miniproject.model.SubscriptionStatus;
import com.kit.miniproject.model.SubscriptionTime;

import java.sql.Timestamp;
import java.util.List;

public interface SubscriptionService {

    Subscription save(Subscription subscription);

    SubscriptionStatus readSubscriptionStatus(Subscription subscription);

    List<Subscription> readAllSubscribedByDate(Timestamp date, SubscriptionTime subscriptionTime);

}
