package com.kit.miniproject.service;

import com.kit.miniproject.model.SubscriptionStatus;
import com.kit.miniproject.model.SubscriptionTime;
import com.kit.miniproject.model.User;

import java.sql.Timestamp;
import java.util.List;

public interface UserService {

    String save(User user);

    User read(String username);

    SubscriptionStatus readSubscription(String username);

    List<User> readAllSubscribedUsers(Timestamp date,
                                      Integer page,
                                      Integer pageSize,
                                      SubscriptionTime subscriptionTime);

}
