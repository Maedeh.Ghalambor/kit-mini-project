package com.kit.miniproject.service.impl;

import com.kit.miniproject.exception.BusinessException;
import com.kit.miniproject.exception.NotFoundException;
import com.kit.miniproject.model.Subscription;
import com.kit.miniproject.model.SubscriptionStatus;
import com.kit.miniproject.model.SubscriptionTime;
import com.kit.miniproject.model.User;
import com.kit.miniproject.repository.UserRepository;
import com.kit.miniproject.service.SubscriptionService;
import com.kit.miniproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import static com.kit.miniproject.util.PaginationUtil.calculateFromIndex;
import static com.kit.miniproject.util.PaginationUtil.calculateToIndex;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private SubscriptionService subscriptionService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           SubscriptionService subscriptionService) {
        this.userRepository = userRepository;
        this.subscriptionService = subscriptionService;
    }

    @Override
    public String save(User user) {
        if (user != null) {
            if (user.getSubscription() != null) {
                user.setSubscription(subscriptionService.save(user.getSubscription()));
            }
            return userRepository.save(user).getName();
        } else {
            throw new BusinessException("Invalid User Information!");
        }
    }

    @Override
    public User read(String username) {
        return userRepository.findByName(username)
                .orElseThrow(() -> new NotFoundException("User Not Found!"));
    }

    @Override
    public SubscriptionStatus readSubscription(String username) {
        User user = read(username);
        return subscriptionService.readSubscriptionStatus(user.getSubscription());
    }

    @Override
    public List<User> readAllSubscribedUsers(Timestamp date,
                                             Integer page,
                                             Integer pageSize,
                                             SubscriptionTime subscriptionTime) {
        List<User> users = null;

        List<Subscription> subscriptions = subscriptionService
                .readAllSubscribedByDate(date, subscriptionTime);

        if (subscriptions != null && !subscriptions.isEmpty()) {
            users = subscriptions.stream()
                    .map(Subscription::getUser)
                    .collect(Collectors.toList())
                    .subList(calculateFromIndex(page, pageSize), calculateToIndex(page, pageSize));
        }

        return users;
    }
}
