package com.kit.miniproject.service.impl;

import com.kit.miniproject.model.SubscriptionTime;
import com.kit.miniproject.model.Subscription;
import com.kit.miniproject.model.SubscriptionStatus;
import com.kit.miniproject.repository.SubscriptionRepository;
import com.kit.miniproject.service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.kit.miniproject.util.TimeUtil.getCurrentTimeInTimestampFormat;

@Service
@Transactional
public class SubscriptionServiceImpl implements SubscriptionService {

    private SubscriptionRepository subscriptionRepository;

    @Autowired
    public SubscriptionServiceImpl(SubscriptionRepository subscriptionRepository) {
        this.subscriptionRepository = subscriptionRepository;
    }

    @Override
    public Subscription save(Subscription subscription) {
        Subscription savedSubscription = null;

        if (subscription != null && subscription.getStatus() != null) {
            validateSubscriptionStatus(subscription.getStatus());
            if (subscription.getStatus().equals(SubscriptionStatus.SUBSCRIBED)) {
                subscription.setSubscriptionDate(getCurrentTimeInTimestampFormat());
                savedSubscription = subscriptionRepository.save(subscription);
            }
        }
        return savedSubscription;
    }

    @Override
    public SubscriptionStatus readSubscriptionStatus(Subscription subscription) {
        return (subscription != null && subscription.getStatus() != null) ?
                subscription.getStatus() :
                SubscriptionStatus.UNSUBSCRIBED;
    }

    @Override
    public List<Subscription> readAllSubscribedByDate(Timestamp date, SubscriptionTime subscriptionTime) {
        validateSubscriptionTime(subscriptionTime);

        return subscriptionRepository.findAll()
                .stream()
                .filter(subscription -> subscription.getStatus().equals(SubscriptionStatus.SUBSCRIBED))
                .filter(subscription -> SubscriptionTime.BEFORE.equals(subscriptionTime)
                        ? subscription.getSubscriptionDate().before(date)
                        : subscription.getSubscriptionDate().after(date))
                .collect(Collectors.toList());
    }

    private void validateSubscriptionStatus(SubscriptionStatus status) {
        if (Arrays.stream(SubscriptionStatus.values()).noneMatch(s -> s.equals(status))) {
            throw new IllegalArgumentException("Invalid Subscription Status!");
        }
    }

    private void validateSubscriptionTime(SubscriptionTime subscriptionTime) {
        if (Arrays.stream(SubscriptionTime.values()).noneMatch(s -> s.equals(subscriptionTime))) {
            throw new IllegalArgumentException("Invalid Subscription Time!");
        }
    }
}
