package com.kit.miniproject.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "subscription")
public class Subscription {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "status")
    private SubscriptionStatus status;

    @Column(name = "subscription_date")
    private Timestamp subscriptionDate;

    @Column(name = "un_subscription_date")
    private Timestamp unSubscriptionDate;

    @OneToOne
    private User user;

    public Subscription(SubscriptionStatus status) {
        this.status = status;
    }
}
