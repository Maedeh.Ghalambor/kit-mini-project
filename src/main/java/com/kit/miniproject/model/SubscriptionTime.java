package com.kit.miniproject.model;

public enum SubscriptionTime {
    BEFORE,
    AFTER;
}
