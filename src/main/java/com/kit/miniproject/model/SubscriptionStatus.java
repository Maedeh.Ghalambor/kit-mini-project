package com.kit.miniproject.model;

public enum SubscriptionStatus {
    SUBSCRIBED,
    UNSUBSCRIBED;
}
