package com.kit.miniproject.util;

public class PaginationUtil {
    public static Integer calculateFromIndex(Integer page, Integer pageSize) {
        if (page == null
                || pageSize == null
                || page == 0
                || pageSize > 200)
            throw new IllegalArgumentException("Invalid Pagination!");

        return (page - 1) * pageSize;
    }

    public static Integer calculateToIndex(Integer page, Integer pageSize) {
        if (page == null
                || pageSize == null
                || page == 0
                || pageSize > 200)
            throw new IllegalArgumentException("Invalid Pagination!");

        return (page * pageSize) - 1;
    }
}
