package com.kit.miniproject.util;

import java.sql.Timestamp;

public class TimeUtil {
    public static Timestamp getCurrentTimeInTimestampFormat() {
        return new Timestamp(System.currentTimeMillis());
    }
}
