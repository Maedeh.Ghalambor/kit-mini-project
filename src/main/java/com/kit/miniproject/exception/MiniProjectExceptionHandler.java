package com.kit.miniproject.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class MiniProjectExceptionHandler {

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<String> exceptionHandler(BusinessException exception) {

        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

        if (exception instanceof BusinessException){
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        } else {
            // todo : other types for bigger projects
        }

        return new ResponseEntity<>(exception.getMessage(), status);
    }
}
