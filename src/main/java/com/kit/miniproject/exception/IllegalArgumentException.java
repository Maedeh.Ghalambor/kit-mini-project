package com.kit.miniproject.exception;

public class IllegalArgumentException extends BusinessException {
    public IllegalArgumentException(String message) {
        super(message);
    }
}
