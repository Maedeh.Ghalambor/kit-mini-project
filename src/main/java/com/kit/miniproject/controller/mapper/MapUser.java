package com.kit.miniproject.controller.mapper;

import com.kit.miniproject.controller.dto.CreateUserRequest;
import com.kit.miniproject.controller.dto.ReadAllSubscribedUsersResponse;
import com.kit.miniproject.model.Subscription;
import com.kit.miniproject.model.User;

import java.util.List;

public class MapUser {

    public static User map(CreateUserRequest request) {
        User user = null;
        if (request != null) {
            user = new User();
            user.setName(request.getUsername());
            user.setPassword(request.getPassword());
            user.setEmail(request.getEmail());
            user.setSubscription(new Subscription(request.getSubscriptionStatus()));
        }
        return user;
    }

    public static List<ReadAllSubscribedUsersResponse> map(List<User> users) {
        List<ReadAllSubscribedUsersResponse> responseList = null;
        if (users != null && !users.isEmpty()) {
            users.forEach(user -> responseList.add(map(user)));
        }
        return responseList;
    }

    private static ReadAllSubscribedUsersResponse map(User user) {
        ReadAllSubscribedUsersResponse response = null;
        if (user != null)
            response = new ReadAllSubscribedUsersResponse(user.getName(), user.getEmail());
        return response;
    }

}
