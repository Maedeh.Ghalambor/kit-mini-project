package com.kit.miniproject.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReadAllSubscribedUsersResponse {
    private String name;
    private String email;
}
