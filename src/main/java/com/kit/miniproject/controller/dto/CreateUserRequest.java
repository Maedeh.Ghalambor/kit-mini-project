package com.kit.miniproject.controller.dto;

import com.kit.miniproject.model.SubscriptionStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserRequest {

    @NotNull
    @NotEmpty
    @ApiModelProperty(required = true)
    private String username;

    @NotEmpty
    @ApiModelProperty(required = true)
    private String password;

    @NotEmpty
    @ApiModelProperty(required = true, example = "maedeh.1989@gmail.com")
    private String email;

    @ApiModelProperty(example = "SUBSCRIBED, UNSUBSCRIBED")
    private SubscriptionStatus subscriptionStatus;

}
