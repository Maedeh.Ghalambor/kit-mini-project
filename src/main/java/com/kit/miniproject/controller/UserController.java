package com.kit.miniproject.controller;

import com.kit.miniproject.controller.dto.CreateUserRequest;
import com.kit.miniproject.controller.dto.CreateUserResponse;
import com.kit.miniproject.controller.dto.ReadAllSubscribedUsersResponse;
import com.kit.miniproject.model.SubscriptionStatus;
import com.kit.miniproject.model.SubscriptionTime;
import com.kit.miniproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import java.sql.Timestamp;
import java.util.List;

import static com.kit.miniproject.controller.mapper.MapUser.*;

@RestController
@RequestMapping("/users")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<CreateUserResponse> create(@Valid @RequestBody CreateUserRequest request) {
        return new ResponseEntity<>(new CreateUserResponse(userService.save(map(request))), HttpStatus.OK);
    }

    @GetMapping("/is-subscribed/{username}")
    public ResponseEntity<SubscriptionStatus> readSubscription(@PathVariable @NotNull String username) {
        return new ResponseEntity<>(userService.readSubscription(username), HttpStatus.OK);
    }

    @GetMapping("/{date}")
    public ResponseEntity<List<ReadAllSubscribedUsersResponse>> readAllSubscribedUsers
            (@PathVariable @NotNull String date,
             @RequestParam(required = false, defaultValue = "1") @Min(1) Integer page,
             @RequestParam(required = false, defaultValue = "10") @Max(200) @Min(1) Integer pageSize,
             @RequestParam(required = false, defaultValue = "AFTER") SubscriptionTime subscriptionTime) {

        return new ResponseEntity<>(map(userService.readAllSubscribedUsers(Timestamp.valueOf(date),
                page,
                pageSize,
                subscriptionTime)), HttpStatus.OK);
    }

}
