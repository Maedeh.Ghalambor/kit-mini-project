# Mini Project For Kit

this is the mini project, made by Maedeh Ghalambor.

Note:
- you may use the swagger on this url : http://localhost:8080//swagger-ui.html
- for simplicity, all the Api s are located under one controller, named "/users"

- you would need to save some users first (using create service), and then call the second and third Api s (readSubscription, readAllSubscribedUsers).

- I used H2 as you asked, but I didn't mention the scripts needed to make the database, as I am sure you already have them.

- for the sake of using java 8, the third Api is reading all subscriptions and then performing some processing using Streams.
- For the third Api, you may use "date" in the format of YYYY-MM-DD hh:mm:ss

- there is much room for improvement, but since this is a sample project, I didn't have time to put more effort for some parts like exception handling and error messages.

